//
//  TTLog.h
//  TTCI
//
//  Created by 姜腾 on 2023/5/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TTLog : NSObject

- (void)log:(NSString *)log;
@end

NS_ASSUME_NONNULL_END
