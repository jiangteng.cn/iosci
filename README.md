# TTCI

[![CI Status](https://img.shields.io/travis/jiangteng/TTCI.svg?style=flat)](https://travis-ci.org/jiangteng/TTCI)
[![Version](https://img.shields.io/cocoapods/v/TTCI.svg?style=flat)](https://cocoapods.org/pods/TTCI)
[![License](https://img.shields.io/cocoapods/l/TTCI.svg?style=flat)](https://cocoapods.org/pods/TTCI)
[![Platform](https://img.shields.io/cocoapods/p/TTCI.svg?style=flat)](https://cocoapods.org/pods/TTCI)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TTCI is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TTCI'
```

## Author

jiangteng, jiangteng@hypergryph.com

## License

TTCI is available under the MIT license. See the LICENSE file for more info.
