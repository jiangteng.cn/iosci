//
//  TTAppDelegate.h
//  TTCI
//
//  Created by jiangteng on 05/06/2023.
//  Copyright (c) 2023 jiangteng. All rights reserved.
//

@import UIKit;

@interface TTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
