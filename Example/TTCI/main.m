//
//  main.m
//  TTCI
//
//  Created by jiangteng on 05/06/2023.
//  Copyright (c) 2023 jiangteng. All rights reserved.
//

@import UIKit;
#import "TTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTAppDelegate class]));
    }
}
